//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_KERNEL_H
#define MASTERSPARK_GPU_KERNEL_H


#include "vector.h"
#include "camera.h"
#include "renderobject.h"
#include "bvh.h"

SHARED bool RayAABBIntersect(const Ray *ray, __global const AABB *box) {
    /*Vector tmin, tmax;
    tmin = (box->min - ray->ro) / ray->rd;
    tmax = (box->max - ray->ro) / ray->rd;
    float temp;
    if (tmin.x > tmax.x) {
        temp = tmin.x;
        tmin.x = tmax.x;
        tmax.x = temp;
    }
    if (tmin.y > tmax.y) {
        temp = tmin.y;
        tmin.y = tmax.y;
        tmax.y = temp;
    }
    if (tmin.z > tmax.z) {
        temp = tmin.z;
        tmin.x = tmax.z;
        tmax.x = temp;
    }
    double x = max(tmin.x, max(tmin.y, tmin.z));
    double y = min(tmax.x, min(tmax.y, tmax.z));
    if(x >= y)
        return false;
    return x > eps || y > eps;*/
    float tmin = (box->min.x - ray->ro.x) / ray->rd.x;
    float tmax = (box->max.x - ray->ro.x) / ray->rd.x;
    if (tmin > tmax)
        kswap(tmin, tmax);
    float tymin = (box->min.y - ray->ro.y) / ray->rd.y;
    float tymax = (box->max.y - ray->ro.y) / ray->rd.y;
    if (tymin > tymax)
        kswap(tymin, tymax);
    if (tmin > tymax || tymin > tmax)
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;
    float tzmin = (box->min.z - ray->ro.z) / ray->rd.z;
    float tzmax = (box->max.z - ray->ro.z) / ray->rd.z;

    if (tzmin > tzmax) kswap(tzmin, tzmax);
    if (tmin > tzmax || tzmin > tmax)
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    return tmin > eps || tmax > eps;
}

typedef struct Intersection {
    int id;
    float distance;
    Vector norm;
} Intersection;

void initIntersection(Intersection *intersection) {
    intersection->distance = -1000;
    intersection->id = -1;
}

void updateIntersection(Intersection *intersection,
                        int id,
                        float distance,
                        Vector *norm) {
    bool f = false;
    if (distance > eps) {
        if (intersection->distance < eps) {
            f = true;
        } else if (distance < intersection->distance) {
            f = true;
        }
    }
    if (f) {
        intersection->distance = distance;
        intersection->id = id;
        intersection->norm = *norm;
    }
}

inline bool hit(Intersection *intersection) {
    return intersection->distance > eps;
}

inline float lengthSquared(Vector v) {
    return dot(v, v);
}

void intersectSphere(
        __global const RenderObject *object,
        const Ray *ray,
        Intersection *intersection) {
    const float a = 1;
    const float r = object->sphere.radius;
    Vector dir = ray->ro - object->sphere.center;
    float b = 2 * dot(ray->rd, dir);//2 * (ray.direction * (ray.origin - center));
    float c = dot(dir, dir)
              - r * r;
    float delta = b * b - 4 * a * c;
    if (delta < 0) { return; }
    else {
        delta = sqrt(delta);
        float d1 = (-b + delta) / (2 * a);
        float d2 = (-b - delta) / (2 * a);
        float d;
        if (d1 < 0)
            d = d2;
        else if (d2 < 0)
            d = d1;
        else {
            d = min(d1, d2);
        }
        if (d < eps) {
            return;
        }
        Vector i = ray->ro + d * ray->rd;
        Vector norm = normalize(i - object->sphere.center);
        if (dot(norm, ray->rd) > 0) {
            norm *= -1;
        }
        updateIntersection(intersection, object->id, d, &norm);
    }
}

#if 1

//https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
void intersectTriangle(
        __global const RenderObject *object,
        const Ray *ray,
        Intersection *intersection) {
    Vector vertex0 = object->triangle.vertex0;
    Vector vertex1 = object->triangle.vertex1;
    Vector vertex2 = object->triangle.vertex2;
    Vector edge1, edge2, h, s, q;
    float a, f, u, v;
    edge1 = vertex1 - vertex0;
    edge2 = vertex2 - vertex0;
    h = cross(ray->rd, edge2);
    a = dot(edge1, h);
    if (a > -0.0001 && a < 0.0001) {
        return;
    }
    f = 1.0f / a;
    s = ray->ro - vertex0;
    u = f * (dot(s, h));
    if (u < 0.0 || u > 1.0)
        return;
    q = cross(s, edge1);
    v = f * dot(ray->rd, q);
    if (v < 0.0 || u + v > 1.0001)
        return;
    float t = f * dot(edge2, q);
    if (t > eps) {
        Vector norm = object->triangle.norm;
        if (dot(norm, ray->rd) > 0) {
            norm *= -1;
        }
        updateIntersection(intersection, object->id, t, &norm);
    }
}

#else

void intersectTriangle(
        __global const RenderObject *object,
        const Ray *ray,
        Intersection *intersection) {
    Vector vertex0 = object->triangle.vertex0;
    Vector vertex1 = object->triangle.vertex1;
    Vector vertex2 = object->triangle.vertex2;
    Vector edge0 = vertex1 - vertex0;
    Vector edge1 = vertex2 - vertex1;
    Vector edge2 = vertex0 - vertex2;
    Vector norm = object->triangle.norm;
    if (fabs(dot(norm, ray->rd)) < 0.001) {
        return;
    }
    float t = -dot(norm, ray->ro - vertex0) / dot(norm, ray->rd);
    if (t < 0)return;
    Vector p = ray->ro + t * ray->rd;
    if (dot(norm, cross(edge0, p - vertex0)) < 0)
        return;
    if (dot(norm, cross(edge1, p - vertex1)) < 0)
        return;
    if (dot(norm, cross(edge2, p - vertex2)) < 0)
        return;
    norm = object->triangle.norm;
    if (dot(norm, ray->rd) > 0) {
        norm *= -1;
    }
    updateIntersection(intersection, object->id, t, &norm);
}
#endif

void reflect(const Vector *n, Vector *v) {
    *v = normalize(*v - 2.0 * *n * (dot(*v, *n)));
}

void updateSeed(__global Seed *seed) {
    seed[0] = 36969 * ((seed[0]) & 65535) + ((seed[0]) >> 16);
    seed[1] = 18000 * ((seed[1]) & 65535) + ((seed[1]) >> 16);
}

float GetRandom(Seed *seed) {
    seed[0] = 36969 * ((seed[0]) & 65535) + ((seed[0]) >> 16);
    seed[1] = 18000 * ((seed[1]) & 65535) + ((seed[1]) >> 16);

    unsigned int ires = ((seed[0]) << 16) + (seed[1]);

    /* Convert to float */
    union {
        float f;
        unsigned int ui;
    } res;
    res.ui = (ires & 0x007fffff) | 0x40000000;

    return (res.f - 2.f) / 2.f;
}

float rand(Seed *Xi) {
    return GetRandom(Xi);
}

Vector randomVectorInHemisphere(const Vector *norm, Seed *Xi) {
    float r1 = 2 * (float) M_PI * rand(Xi), r2 = rand(Xi), r2s = (float) sqrt(r2);
    float x = norm->x;
    Vector u = normalize(cross((fabs(x) > 0.1)
                               ? vec3(0, 1, 0)
                               : vec3(1, 0, 0), *norm));
    Vector v = cross(*norm, u);
    Vector r = normalize(u * cos(r1) * r2s + v * sin(r1) * r2s + *norm * sqrt(1 - r2));
    return r;
}

Vector randomVectorInSphere(Seed *Xi) {
    const float u1 = rand(Xi);
    const float u2 = rand(Xi);
    const float zz = 1.f - 2.f * u1;
    const float r = sqrt(max(0.f, 1.f - zz * zz));
    const float phi = 2.f * M_PI * u2;
    const float xx = r * cos(phi);
    const float yy = r * sin(phi);
    return vec3(xx, yy, zz);
}

void BVHTraversal(const Ray *ray,
                  Intersection *intersection,
                  __global const BVHNode *bvhNode,
                  __global const RenderObject *objects) {
    int stack[MAX_BVH_DEPTH * 2];
    int sp = 0;
    int ptr = 0;
    stack[sp++] = 0;// push root
    do {
        //pop
        ptr = stack[--sp];
        __global const BVHNode *node = bvhNode + ptr;
        if (RayAABBIntersect(ray, &node->box)) {
            // intersects
            // see if it is a leaf node
            int cnt = node->cnt;
            __global const int *primitive = node->primitive;
            stack[sp++] = getLeft(ptr);
            stack[sp++] = getRight(ptr);
            if (cnt) {
                for (int i = 0; i < cnt; i++) {
                    __global const RenderObject *object = objects + *primitive++;
                    if (object->type == TRIANGLE) {
                        intersectTriangle(object, ray, intersection);
                    } else {
                        intersectSphere(object, ray, intersection);
                    }
                }
                sp -= 2;
            }
        }
    } while (sp > 0);
}

void _BVHTraversal(const Ray *ray,
                   Intersection *intersection,
                   __global const BVHNode *bvhNode,
                   __global const RenderObject *objects) {
    int stack[MAX_BVH_DEPTH * 2];
    int sp = 0;
    int ptr = 0;
    stack[sp++] = 0;// push root
    if (!RayAABBIntersect(ray, &bvhNode[0].box)) {
        return;
    }
    do {
        //pop
        ptr = stack[--sp];
        __global const BVHNode *node = bvhNode + ptr;
        int cnt = node->cnt;
        __global const int *primitive = node->primitive;
        if (cnt) {
            for (int i = 0; i < cnt; i++) {
                __global const RenderObject *object = objects + *primitive++;
                if (object->type == TRIANGLE) {
                    intersectTriangle(object, ray, intersection);
                } else {
                    intersectSphere(object, ray, intersection);
                }
            }
        } else {
            int left = getLeft(ptr);
            int right = getRight(ptr);
            if (RayAABBIntersect(ray, &bvhNode[left].box)) {
                stack[sp++] = left;
            }
            if (RayAABBIntersect(ray, &bvhNode[right].box)) {
                stack[sp++] = right;
            }
        }
    } while (sp > 0);
}


//fast ray tracing, direct lighting
Vector rayTrace(Ray ray,
                __global BVHNode *bvhNode,
                __global const RenderObject *objects,
                __global int *objectCount,
                Seed *Xi) {
    Vector color = vec3(0, 0, 0);
    Intersection intersection;
    initIntersection(&intersection);
    BVHTraversal(&ray, &intersection, bvhNode, objects);
    if (!hit(&intersection)) {
        return color;
    }
    __global const RenderObject *object = objects + intersection.id;
    Vector emit = object->material.Ka;
    if (dot(emit, emit) > 1) { //hit light source
        return emit;
    }
    Vector dir = -normalize(vec3(1, -1, 0));
    return object->material.Kd * max(0.2f, dot(dir, intersection.norm));

}

#define MAX_LIGHT_PATH_LENGTH 4
#define MAX_DEPTH 3

typedef struct LightPathNode {
    Vector dir; // from camera to light source
    Vector norm;
    Vector hitPoint;
    Vector color;
    int id;   // index of hit object

} LightPathNode;

void generatingLightPath(__global BVHNode *bvhNode,
                         __global const RenderObject *objects,
                         __global const int *lights,
                         __global int *lightCount,
                         Seed *Xi, LightPathNode *lightPath, int *count) {
    if (MAX_LIGHT_PATH_LENGTH < 1)
        return;
    int nodeCnt = 0;
    Ray shootRay;
    Vector refl = vec3(1, 1, 1);
    // shoot rays
    int idx = (int) ((float) (*lightCount) * rand(Xi));
    idx %= *lightCount;

    __global const RenderObject *light = &objects[lights[idx]];
    Vector norm;
    if (light->type == TRIANGLE) {
        const float lambda = rand(Xi);
        Vector a = light->triangle.vertex1 - light->triangle.vertex0;
        Vector b = light->triangle.vertex2 - light->triangle.vertex0;
        shootRay.ro = light->triangle.vertex0
                      + lambda * a
                      + (1 - lambda) * b;
        norm = light->triangle.norm;
    } else {
        norm = randomVectorInSphere(Xi);
        shootRay.ro = norm * light->sphere.radius + light->sphere.center;
    }
    shootRay.rd = randomVectorInSphere(Xi);
    if (dot(norm, shootRay.rd) < 0) {
        norm *= -1;
    }
    lightPath[0].dir = shootRay.rd;
    lightPath[0].hitPoint = shootRay.ro;
    lightPath[0].id = lights[idx];
    lightPath[0].norm = norm;
    lightPath[0].color = vec3(0, 0, 0);
    for (nodeCnt = 1; nodeCnt < MAX_LIGHT_PATH_LENGTH; nodeCnt++) {

        Intersection intersection;
        initIntersection(&intersection);
        BVHTraversal(&shootRay, &intersection, bvhNode, objects);
        if (!hit(&intersection)) {
            break;
        }
        shootRay.ro += intersection.distance * shootRay.rd;
        if (dot(norm, shootRay.rd) > 0) {
            norm *= -1;
        }
        lightPath[nodeCnt].hitPoint = shootRay.ro;
        lightPath[nodeCnt].dir = shootRay.rd;
        lightPath[nodeCnt].id = intersection.id;
        lightPath[nodeCnt].norm = norm;
        __global const RenderObject *object = &objects[intersection.id];
        norm = intersection.norm;


        Vector diff = object->material.Kd, spec = object->material.Ks;
        // R.R for path determination
        const float p1 = vmax(diff);
        const float p2 = vmax(spec);
        const float total = p1 + p2;
        float prob;
        const float dice = rand(Xi) * total;
        if(total < eps){break;}
        if (dice < p1) {
            shootRay.rd = randomVectorInHemisphere(&norm, Xi);
            prob = p1 / total;
            const Vector BRDF = diff / M_PI;
            lightPath[nodeCnt].color = BRDF / prob * 2 * M_PI;
            refl *= BRDF * -dot(lightPath[nodeCnt].dir, norm) / prob * 2 * M_PI;

        } else {
            reflect(&norm, &shootRay.rd);
            prob = p2 / total;
            lightPath[nodeCnt].color = spec / prob;
            refl *= spec * -dot(lightPath[nodeCnt].dir, norm) / prob;
        }
        // R.R.
        const float P = vmax(refl) * 0.5f;
        if (nodeCnt >= 1) {
            if (rand(Xi) < P) {
                refl *= 1 / P;
            } else {
                nodeCnt++;
                break;
            }
        }
    }
    //  printf("%d\n",nodeCnt);
    *count = nodeCnt;
}

bool testVisible(int fromId, const Vector *fromPoint,
                 int endId, const Vector *endPoint,
                 __global BVHNode *bvhNode,
                 __global const RenderObject *objects) {
    if (fromId == endId)
        return true;
    if (length(*endPoint - *fromPoint) < eps)
        return true;
    Ray ray;
    ray.ro = *fromPoint;
    ray.rd = normalize(*endPoint - *fromPoint);
    ray.ro += 0.001 * ray.rd;
    Intersection intersection;
    initIntersection(&intersection);
    BVHTraversal(&ray, &intersection, bvhNode, objects);
    if (!hit(&intersection))
        return false;
    ray.ro += intersection.distance * ray.rd;
    return intersection.id == endId && length(ray.ro - *endPoint) < 0.1;
}

Vector connectPath(LightPathNode *lightPath,
                   int connectIdx,
                   LightPathNode *cameraPath,
                   int cameraPathCount,
                   __global const RenderObject *objects) {
    Vector endPoint = lightPath[connectIdx].hitPoint;
    Vector color = vec3(0, 0, 0);
    Vector refl = vec3(1, 1, 1);
    float weight = 1;
    for (int i = 0; i < cameraPathCount; i++) {
        if (i > 0)
            refl *= dot(cameraPath[i - 1].dir, cameraPath[i - 1].norm);
        int id = cameraPath[i].id;
        __global const RenderObject *object = &objects[id];
        color += object->material.Ka * refl;
        refl *= cameraPath[i].color;

    }
    Vector v = endPoint - cameraPath[cameraPathCount - 1].hitPoint;
    Vector dir = normalize(v);
    weight *= dot(dir, lightPath[connectIdx].norm);
    weight *= dot(-dir, cameraPath[cameraPathCount - 1].norm) / lengthSquared(v);
    for (int i = connectIdx; i >= 0; i--) {
        int id = lightPath[i].id;
        __global const RenderObject *object = &objects[id];
        Vector ambient = object->material.Ka;
        color += ambient * refl;
        float f = -dot(lightPath[i].dir, lightPath[i].norm);
        refl *= lightPath[i].color * max(0.0f, f);
    }
    return color / (connectIdx + cameraPathCount + 1) * clamp(weight, 0, 1);
}

Vector radiance(Ray ray,
                __global BVHNode *bvhNode,
                __global const RenderObject *objects,
                __global const int *lights,
                __global int *lightCount,
                Seed *Xi) {
    // therefore the scene must have at least one light source
    Vector color = vec3(0, 0, 0);
    Vector refl = vec3(1, 1, 1);
    Vector Le = vec3(0, 0, 0);
    Intersection intersection;
    LightPathNode lightPath[MAX_LIGHT_PATH_LENGTH], cameraPath[MAX_DEPTH];
    int lightPathCount;
    int cameraPathCount = 0;
    generatingLightPath(bvhNode, objects, lights, lightCount, Xi, lightPath, &lightPathCount);
    int connect = 0;
    int connectIdx = 0;
    Vector endPoint;
    int endId;
    for (; cameraPathCount < MAX_DEPTH; cameraPathCount++) {
        initIntersection(&intersection);
        BVHTraversal(&ray, &intersection, bvhNode, objects);
        if (!hit(&intersection)) {
            break;
        }
        Vector norm = intersection.norm;
        int id = intersection.id;
        ray.ro += intersection.distance * ray.rd;
        cameraPath[cameraPathCount].id = intersection.id;
        cameraPath[cameraPathCount].hitPoint = ray.ro;
        cameraPath[cameraPathCount].norm = norm;
        __global const RenderObject *object = &objects[id];
        Vector ambient = object->material.Ka;
        if (length(ambient) > 1) { //hit light source
            color += ambient * refl;
            Le = color;
            break;
        }
        // continue tracing
        // R.R for path determination
        Vector diff = object->material.Kd, spec = object->material.Ks;
        const float p1 = vmax(diff);
        const float p2 = vmax(spec);
        const float total = p1 + p2;
        float prob;
        const float dice = rand(Xi) * total;
        if (total < eps) {
            break;
        }
        if (dot(ray.rd, norm) > 0) {
            norm *= -1;
        }
        if (dice < p1) {
            ray.rd = randomVectorInHemisphere(&norm, Xi);
            prob = p1 / total;
            const Vector BRDF = diff / M_PI;
            cameraPath[cameraPathCount].color = BRDF / prob * 2 * M_PI;
            refl *= BRDF * dot(ray.rd, norm) / prob * 2 * M_PI;

        } else {
            reflect(&intersection.norm, &ray.rd);
            prob = p2 / total;
            cameraPath[cameraPathCount].color = spec / prob;
            refl *= spec * dot(ray.rd, norm) / prob;
        }
        cameraPath[cameraPathCount].dir = ray.rd;

        // checking visiblity
        for (int i = 0; i < lightPathCount; i++) {
            connectIdx = i;
            endPoint = lightPath[connectIdx].hitPoint;
            endId = lightPath[connectIdx].id;
            // printf("test %d %d\n", id ,endId);
            if (testVisible(id, &ray.ro, endId, &endPoint, bvhNode, objects)) {
                connect ++ ;
                Vector c = connectPath(lightPath, connectIdx, cameraPath, cameraPathCount + 1, objects);
                Le += c;
            }
        }
        ray.ro += 2 * eps * ray.rd;

    }
    return Le / (1 + connect);
}
#define PATH_AMBIENT 1
#define PATH_DIFFUSE 2
#define PATH_SPEC 3
float RRNewRay(Ray *ray,
               Vector *norm,
               const Vector ambient,
               const Vector diffuse,
               const Vector specular,
               Seed *Xi,
               Vector *color, int *path) {
    const float p0 = vmax(ambient);
    const float p1 = vmax(diffuse);
    const float p2 = vmax(specular);
    const float total = p0 + p1 + p2;
    float prob;
    const float dice = rand(Xi) * total;
    if(total < eps){
        *path  = PATH_AMBIENT;
        *color = vec3(0,0,0);
        prob = 1;
    }
    if(dice < p0){
        prob = p0 / total;
        *color = ambient;
        *path = PATH_AMBIENT;
    }else if (dice < p0 + p1) {
        ray->rd = randomVectorInHemisphere(norm, Xi);
        prob = p1 / total;
        *color = diffuse / M_PI * 2 * M_PI;
        *path = PATH_DIFFUSE;
    } else {
        reflect(norm, &ray->rd);
        prob = p2 / total;
        *color = specular;
        *path = PATH_SPEC;
    }
    return prob;
}

bool visible(Vector point, __global BVHNode *bvhNode,
             __global const RenderObject *objects,
             int id, Vector targetPoint, Vector *dir) {
    Ray ray;
    ray.ro = point;
    ray.rd = normalize(targetPoint - point);
    ray.ro += 0.01 * ray.rd;
    Intersection intersection;
    initIntersection(&intersection);
    BVHTraversal(&ray, &intersection, bvhNode, objects);
    ray.ro += intersection.distance * ray.rd;
    if (!hit(&intersection) || id != intersection.id || length(ray.ro - targetPoint) > 0.01) {
        return false;
    }
    *dir = ray.rd;
    return true;
}

Vector sampleLights(int id, const Vector *point,
                    const Vector *n,
                    __global BVHNode *bvhNode,
                    __global const RenderObject *objects,
                    __global const int *lights,
                    __global int *lightCount,
                    Seed *Xi) {

    int cnt = *lightCount;
    Vector color = vec3(0, 0, 0);
    for (int i = 0; i < cnt; i++) {

        float lambda = rand(Xi);
        float A;
        __global const RenderObject *object = &objects[lights[i]];
        if(id == lights[i]){
            return object->material.Ka;
        }
        Vector o;
        Vector norm;
        if (object->type == TRIANGLE) {
            Vector a = object->triangle.vertex1 - object->triangle.vertex0;
            Vector b = object->triangle.vertex2 - object->triangle.vertex0;
            A = length(cross(a, b)) * 0.5;
            o = object->triangle.vertex0
                + lambda * a
                + (1 - lambda) * b;
            norm = object->triangle.norm;
        } else {
            A = M_PI * object->sphere.radius * object->sphere.radius;
            norm = randomVectorInSphere(Xi);
            o = norm * object->sphere.radius + object->sphere.center;
        }
        Vector dir;

        float len = lengthSquared(o - *point);
        // if (visible(o, bvhNode, objects,id, *point, &dir)) {
        if (  visible(*point, bvhNode, objects, lights[i], o, &dir)) {
            dir *= -1;
            if (dot(norm, dir) < 0) {
                norm *= -1;
            }

            color += A / len * max(0.0f, dot(dir, norm))
                     * object->material.Ka * max(0.0f, dot(dir, -*n));
        }
    }
    return min(color, vec3(1, 1, 1));
}


Vector radianceDirect(Ray ray,
                      __global BVHNode *bvhNode,
                      __global const RenderObject *objects,
                      __global const int *lights,
                      __global int *lightCount,
                      Seed *Xi) {
    Vector color = vec3(0, 0, 0);
    Vector refl = vec3(1, 1, 1);
    int depth = 0;
    const float p = 1.0f / (2.0f * M_PI);
    bool s = true;
    while (depth < 5) {
        Intersection intersection;
        initIntersection(&intersection);
        float P = vmax(refl);
        if (rand(Xi) < P) {
            refl /= P;
        } else {
            break;
        }
        if (vmax(color) >= 1) {
            break;
        }

        BVHTraversal(&ray, &intersection, bvhNode, objects);
        if (!hit(&intersection)) {
            break;
        }
        __global const RenderObject *object = objects + intersection.id;
        Vector ambient = object->material.Ka;


        Vector BRDF;
        ray.ro += intersection.distance * ray.rd;

        if (dot(ray.rd, intersection.norm) > 0) {
            intersection.norm *= -1;
        }
        int path;

        float prob = RRNewRay(&ray,
                              &intersection.norm,
                              ambient,
                              object->material.Kd,
                              object->material.Ks,
                              Xi, &BRDF, &path);
        if(path == PATH_AMBIENT){
            return color + ambient * refl * s;
        }
        const float cos_theta = dot(ray.rd, intersection.norm);
        Vector direct = vec3(0, 0, 0);
        if(path == PATH_DIFFUSE) {
            s = false;
            direct = sampleLights(intersection.id, &ray.ro, &intersection.norm, bvhNode, objects, lights, lightCount,
                                  Xi);
        }
        refl *= cos_theta * BRDF / prob;
        color += direct * refl;
        ray.ro +=  4 * eps * ray.rd; // magic
        depth++;
    }
    return min(color,vec3(3,3,3));
}


#endif //MASTERSPARK_GPU_KERNEL_H
