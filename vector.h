//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_VEC_H
#define MASTERSPARK_GPU_VEC_H

#include "common.h"
#define vassign(v,a,b,c) v = vec3((a),(b),(c))
#define v_op(op,a,b,c) vassign((c),(a).x op (b).x,(a).y op (b).y,(a).z op (b).z)
#define vadd(a,b,c) v_op(+,(a),(b),(c))
#define vsub(a,b,c) v_op(-,(a),(b),(c))
#define vmul(a,b,c) v_op(*,(a),(b),(c))
#define vdiv(a,b,c) v_op(/,(a),(b),(c))
#define vsmul(a,k,b) vassign(b,(a).x*(k),(a).y*(k),(a).z*(k))
#define vcross(a, b, v) vassign((v), (a).y * (b).z - (a).z * (b).y, (a).z * (b).x - (a).x * (b).z, (a).x * (b).y - (a).y * (b).x)
#define vmax(v) ((v).x > (v).y ? ((v).x > (v).z ? (v).x : (v).z) :((v).y > (v).z ? (v).y : (v).z))
#define vdot(a,b) ((a).x * (b).x + (a).y * (b).y + (a).z * (b).z)
#define toInt(x) (clamp((x),0.0,1.0) * 255.0)
typedef unsigned int  Seed;
#ifndef GPU_KERNEL

#include <CL/cl.h>
typedef cl_float3 Vector;
SHARED Vector vec3(float a,float b,float c){
    return (Vector){a,b,c};
}
SHARED Vector min(Vector a, Vector b){
    return vec3(kmin(a.x,b.x),kmin(a.y,b.y),kmin(a.z,b.z));
}
SHARED Vector max(Vector a, Vector b){
    return vec3(kmax(a.x,b.x),kmax(a.y,b.y),kmax(a.z,b.z));
}
SHARED Vector normalize(Vector v){
    float len = sqrt(vdot(v,v));
    vsmul(v,1/len,v);
    return v;
}
#else
typedef float3 Vector;
inline Vector vec3(float a,float b,float c){
    return (Vector)(a,b,c);
}
#endif

typedef struct Ray {
    Vector ro;
    Vector rd;
} Ray;

SHARED float getAxis(const Vector v,int k){
    if(k == 0){
        return v.x;
    }else if(k == 1){
        return v.y;
    }else if(k == 2){
        return v.z;
    }else{
        return 0;
    }
}

SHARED Vector vecRotate(const Vector x, const Vector axis, const float angle) {
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0f - c;
    Vector u = vec3(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s,
                    oc * axis.z * axis.x + axis.y * s);
    Vector v=vec3(oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c,
                  oc * axis.y * axis.z - axis.x * s);
    Vector w=vec3(oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s,
                  oc * axis.z * axis.z+ c);
    return vec3(vdot(u, x), vdot(v, x), vdot(w,x));
}

#endif //MASTERSPARK_GPU_VEC_H
