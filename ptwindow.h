//
// Created by xiaoc on 2018/10/31.
//

#ifndef MASTERSPARK_PTWINDOW_H
#define MASTERSPARK_PTWINDOW_H

#include "util.h"
#include "window.h"
#include "scene.h"

void keyCallBack(GLFWwindow *window, int key, int scancode, int action, int mods);

class PathTracerWindow : public Window {
protected:
    Scene scene;
    int spp;
    std::vector<unsigned char> pixels;
    volatile bool alive;
    std::string logOutput;
    std::thread renderThread;
    double start;
    void addToDispatch();

public:
    PathTracerWindow(int w, int h, int _spp) : scene(w, h), Window(w, h, "MasterSpark") {
        resize();
        spp = _spp;
        alive = true;

    }
    void setCameraPos(float x, float y, float z){
        scene.camera.origin = vec3(x,y,z);
    }
    void addObject(const RenderObject &object) {
        scene.addObject(object);
    }

    void log(const std::string &s) {
        logOutput.append(s);
    }

    void save();

    void doRender();

    void render();

    static double sec() {
        return (double) clock() / CLOCKS_PER_SEC;
    }

    void show() override ;

    void paintGL() {
        glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
    }

    void update() {
    }

    Scene &getScene() { return scene; }

    void loadObj(const std::string &filename) {
        scene.loadObj(filename);
    }

    void keyEvent(int key, int scancode, int action, int mods) override;

    void resetSample();
};


#endif //MASTERSPARK_PTWINDOW_H
