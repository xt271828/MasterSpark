//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_RENDEROBJECT_H
#define MASTERSPARK_GPU_RENDEROBJECT_H

#include "vector.h"
#include "material.h"
#include "bvh.h"

typedef struct Sphere {
    Vector center;
    float radius;
} Sphere;
typedef struct Triangle {
    Vector vertex0, vertex1, vertex2;
    Vector norm;
} Triangle;
typedef enum ObjectType {
    SPHERE,
    TRIANGLE
} ObjectType;
typedef struct RenderObject {
    ObjectType type;
    unsigned int id;
    union {
        Sphere sphere;
        Triangle triangle;
    };
    Material material;
} RenderObject;

SHARED AABB getBoundBox(const RenderObject object) {
    Vector vmin = vec3(inf,inf,inf), vmax=vec3(-inf,-inf,-inf);
    if (object.type == SPHERE) {
        float r = object.sphere.radius;
        Vector center = object.sphere.center;
        Vector v = vec3(r, r, r);
        vsub(center, v, vmin);
        vadd(center, v, vmax);
    } else if (object.type == TRIANGLE) {
        vmin = min(object.triangle.vertex0,vmin);
        vmin = min(object.triangle.vertex1,vmin);
        vmin = min(object.triangle.vertex2,vmin);
        vmax = max(object.triangle.vertex0,vmax);
        vmax = max(object.triangle.vertex1,vmax);
        vmax = max(object.triangle.vertex2,vmax);
    }
    Vector d = vec3(eps,eps,eps);
    vsub(vmin,d,vmin);
    return(AABB){.min = vmin,.max = vmax};
}

#endif //MASTERSPARK_GPU_RENDEROBJECT_H
