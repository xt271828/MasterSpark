//
// Created by xiaoc on 2018/10/31.
//

#ifndef MASTERSPARK_SCENE_H
#define MASTERSPARK_SCENE_H

#include "util.h"
#include "vector.h"
#include "camera.h"
#include "renderobject.h"
#include "material.h"
#include "bvh.h"
#include <CL/cl.h>
class PathTracerWindow;

class Scene {
    std::vector<Vector> screen;
    std::vector<RenderObject> objects;
    std::vector<int> lights;
    std::vector<unsigned char> pixelBuffer;
    unsigned int w, h;
    double sampleCount;
    bool done;
    Seed* seedBuffer;
    size_t global_item_size,local_work_size;
    cl_context context;
    cl_command_queue command_queue;
    cl_program program;
    cl_kernel kernel;
    cl_mem kCamera,kSampleCount,kObjects,kObjectCount,kBVH,kOutImage,kXi,kLights,kLightCount;
    BVHNode * bvhNode;
    Camera camera;
    volatile bool sampleResetFlag;
    void clSetup();
    void renderPass(Seed * Xi);
    void copyImage();
    void constructBVH();
    void topDownBVH(int ptr,std::vector<RenderObject>&, int axis,int depth);
public:
    void loadObj(const std::string& filename);
    void resetRand();
    void addObject(const RenderObject &object){
        objects.emplace_back(object);
    }
    void loadKernel(const char*);
    friend class PathTracerWindow;

    Scene(int w, int h);


    void renderSamples(int spp, int samples = 1);

    void prepare();

    void save();
    ~Scene(){

    }
};
inline Material makeEmission(const Vector & emittance){
    return {.type = EMIT,.Ka = emittance,.Kd = vec3(0,0,0),.Ks=vec3(0,0,0)};
}
inline Material makeDiffuse(const Vector & refl){
    return {.type = DIFF,.Ka = vec3(0,0,0),.Kd = refl,.Ks=vec3(0,0,0)};
}
inline Material makeSpec(const Vector & refl){
    return {.type = SPEC,.Ka = vec3(0,0,0),.Kd = vec3(0,0,0),.Ks=refl};
}
RenderObject makeSphere(const Vector& center,double radius, const Material &material);
RenderObject makeTriangle(const Vector&,const Vector&,const Vector&,const Material &material);
#endif //MASTERSPARK_SCENE_H
