//
// Created by xiaoc on 2018/11/9.
//

#ifndef MASTERSPARK_GPU_POLYGON_H
#define MASTERSPARK_GPU_POLYGON_H

#include "util.h"
#include "vector.h"
#include "renderobject.h"

struct Polygon {
    std::vector<Vector> vertices;
    std::vector<RenderObject> trigs;
    Material material;

    Polygon() {}

    void addVertex(const Vector &vector) {
        vertices.emplace_back(vector);
    }

    const std::vector<RenderObject> &getTrigs() {
        if (trigs.empty() && vertices.size() >= 3) {
            for (int i = 1; i < vertices.size() - 1; i++) {
                Triangle triangle;
                triangle.vertex0 = vertices[0];
                triangle.vertex1 = vertices[i];
                triangle.vertex2 = vertices[i + 1];
                Vector a, b;
                vsub(triangle.vertex0, triangle.vertex1, a);
                vsub(triangle.vertex0, triangle.vertex2, b);
                vcross(a, b, triangle.norm);
                triangle.norm = normalize(triangle.norm);
                RenderObject object;
                object.material = material;
                object.type = TRIANGLE;
                object.triangle = triangle;
                trigs.emplace_back(object);
            }
        }
        return trigs;
    }
};

#endif //MASTERSPARK_GPU_POLYGON_H
