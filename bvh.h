//
// Created by xiaoc on 2018/11/7.
//

#ifndef MASTERSPARK_GPU_BVH_H
#define MASTERSPARK_GPU_BVH_H

#include "vector.h"

#ifndef GPU_KERNEL

#include <math.h>

#endif

#define MAX_BVH_DEPTH (20)
#define MAX_BVH_CHILD (16)
typedef struct AABB {
    Vector min, max;
} AABB;

typedef struct BVHNode {
    AABB box;
    short cnt;
    int primitive[MAX_BVH_CHILD];
} BVHNode;

SHARED int getLeft(int idx) {
    return (idx <<1) + 1;
}

SHARED int getRight(int idx) {
    return (idx << 1) + 2;
}

SHARED int getParent(int idx) {
    return (idx - 1)/2;
}

SHARED Vector center(AABB box) {
    Vector c;
    vadd(box.min, box.max, c);
    vsmul(c, 0.5, c);
    return c;
}

SHARED Vector size(AABB a) {
    Vector s;
    vsub(a.max, a.min, s);
    return s;
}

SHARED bool AABBIntersect(AABB a, AABB b) {
    Vector s1 = size(a), s2 = size(b);
    Vector c1 = center(a), c2 = center(b);
    return 2 * fabs(c1.x - c2.x) <= fabs(s1.x + s2.x) + eps
           && 2 * fabs(c1.y - c2.y) <= fabs(s1.y + s2.y)+ eps
           && 2 * fabs(c1.z - c2.z) <= fabs(s1.z + s2.z)+ eps;
}

#define kswap(a, b) do{float _t = (a);(a) = (b); b = _t;}while(0)



#endif //MASTERSPARK_GPU_BVH_H
