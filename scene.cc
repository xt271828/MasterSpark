//
// Created by xiaoc on 2018/10/31.
//

#include "scene.h"
#include "polygon.h"

Scene::Scene(int w, int h) {
    done = false;
    screen.resize(w * h);
    sampleCount = 0;
    this->w = w;
    this->h = h;
    sampleResetFlag = false;
    camera.w = w;
    camera.h = h;
    camera.origin = vec3(0, 0, 0);
    camera.dir = vec3(0, 0, 0);
    camera.GI = false;
    loadKernel("kernel.cl");
}

void Scene::renderPass(Seed *Xi) {
    if (sampleResetFlag) {
        sampleCount = 0;
        sampleResetFlag = false;
    }
    float cnt = sampleCount;
    clEnqueueWriteBuffer(command_queue, kSampleCount, CL_TRUE, 0,
                         sizeof(float), &cnt, 0, nullptr, nullptr);
    clEnqueueWriteBuffer(command_queue, kCamera, CL_TRUE, 0, sizeof(Camera),
                         &camera, 0, nullptr, nullptr);
    size_t local_item_size = local_work_size;
    if (global_item_size % local_item_size != 0)
        global_item_size = (global_item_size / local_item_size + 1) * local_item_size;
    cl_int ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, nullptr,
                                        &global_item_size, &local_item_size, 0, nullptr, nullptr);
    if (ret != CL_SUCCESS) {
        fmt::print(stderr, "Cannot Enqueue NDRangeKernel\n");
    }
    clFinish(command_queue);
    sampleCount += 1;
}

void Scene::copyImage() {
    clEnqueueReadBuffer(command_queue, kOutImage, CL_TRUE, 0,
                        w * h * sizeof(Vector), screen.data(), 0, nullptr, nullptr);
}

unsigned int seed() {
    static std::random_device randomDevice;
    static std::mt19937 gen(randomDevice()); //Standard mersenne_twister_engine seeded with rd()
    static std::uniform_int_distribution<unsigned int> dis(0, (0xFFFFFFFF));
    return (unsigned int) dis(gen);
}

void Scene::renderSamples(int spp, int samples) {
    const double sppSqrt = (double) sqrt(spp);
    if (done)
        return;
    for (int k = 0; k < samples && !done; k++) {
        if (sampleResetFlag) {
            renderPass(nullptr);
            break;
        } else {
            renderPass(nullptr);
        }
    }
    copyImage();
    pixelBuffer.clear();
    for (auto &i: screen) {
        pixelBuffer.emplace_back(toInt(i.x));
        pixelBuffer.emplace_back(toInt(i.y));
        pixelBuffer.emplace_back(toInt(i.z));
        pixelBuffer.emplace_back(255);
    }
}

void Scene::prepare() {
    unsigned int id = 0;
    for (auto &object:objects) {
        object.id = id++;
    }
    for (auto &object:objects) {
        auto emit = object.material.Ka;
        if (vdot(emit, emit) > 1) {
            lights.emplace_back(object.id);
        }
    }
    fmt::print("Object count: {}\n", objects.size());
    if(lights.empty()){
        fmt::print(stderr, "no light source\n");
    }
    fmt::print("Light count: {}\n", lights.size());
    constructBVH();
    clSetup();
}


void Scene::save() {
    pixelBuffer.clear();
    for (int i = w - 1; i >= 0; i--)
        for (int j = h - 1; j >= 0; j--) {
            auto x = i;
            auto y = h - 1 - j;
            auto v = screen[x * h + y];
            pixelBuffer.emplace_back(toInt(v.x ));
            pixelBuffer.emplace_back(toInt(v.y));
            pixelBuffer.emplace_back(toInt(v.z));
            pixelBuffer.emplace_back(255);
        }
}

size_t required(size_t n) {
    size_t m = (size_t) ceil(log2(n));
    return std::min(m - 1, (n << 1) - (m >> 1) - 1);
}

#define MAX_SOURCE_SIZE (0x100000)

void Scene::loadKernel(const char *filename) {
    fmt::print("Loading kernel...\"{0}\"\n", filename);
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen(filename, "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel \"%s\".\n", filename);
        exit(-1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);
    cl_platform_id platform_id = nullptr;
    cl_device_id device_id = nullptr;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1,
                         &device_id, &ret_num_devices);
    size_t gsize = 0;


    char *buffer = new char[4096];
    clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(char) * 4096, buffer, nullptr);
    fmt::print("Running on {0}\n", buffer);
    delete[] buffer;
    // Create an OpenCL context
    context = clCreateContext(nullptr,
                              1,
                              &device_id,
                              nullptr,
                              nullptr,
                              &ret);

    // Create a command queue
    command_queue = clCreateCommandQueueWithProperties(
            context,
            device_id,
            nullptr,
            &ret);
    program = clCreateProgramWithSource(context, 1,
                                        (const char **) &source_str, (const size_t *) &source_size, &ret);

    // Build the program
    ret = clBuildProgram(program, 1, &device_id,
                         "-I. -Werror -DGPU_KERNEL -cl-single-precision-constant",
                         nullptr, nullptr);
    if (ret != CL_SUCCESS) {
        fmt::print("Program Build failed\n");
        size_t length;
        char buffer[40960];
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &length);
        fmt::print(stderr, "\"--- Build log ---\n{0}\n", buffer);
        exit(1);
    }
    // Create the OpenCL kernel
    kernel = clCreateKernel(program, "render", &ret);
    ret = clGetKernelWorkGroupInfo(kernel,
                                   device_id,
                                   CL_KERNEL_WORK_GROUP_SIZE,
                                   sizeof(size_t),
                                   &gsize,
                                   NULL);
    fmt::print("work size = {0}\n", gsize);
    local_work_size = gsize;
}

void Scene::clSetup() {
    cl_int ret;
    fmt::print("Allocating CL buffer...\n");
    kCamera = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(Camera), nullptr, &ret);
    kSampleCount = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float), nullptr, &ret);
    kXi = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(Seed) * 2 * w * h, nullptr, &ret);
    kOutImage = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(Vector) * w * h, nullptr, &ret);
    kObjectCount = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int), nullptr, &ret);
    kLightCount = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int), nullptr, &ret);
    kObjects = clCreateBuffer(context,
                              CL_MEM_READ_ONLY,
                              sizeof(RenderObject) * objects.size(),
                              nullptr, &ret);
    kBVH = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(BVHNode) * ((2 << MAX_BVH_DEPTH) + 1),
                          nullptr, &ret);
    kLights = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * lights.size(),
                             nullptr, &ret);
    Camera camera;
    camera.origin = {0, 0, 0};
    camera.w = w;
    camera.h = h;
    seedBuffer = new Seed[w * h * 2];
    resetRand();
    ret = clEnqueueWriteBuffer(command_queue, kXi, CL_TRUE, 0,
                               sizeof(Seed) * w * h * 2,
                               seedBuffer, 0, nullptr, nullptr);
    ret = clEnqueueWriteBuffer(command_queue, kCamera, CL_TRUE, 0,
                               sizeof(Camera), &camera, 0, nullptr, nullptr);
    ret = clEnqueueWriteBuffer(command_queue, kSampleCount, CL_TRUE, 0,
                               sizeof(float), &kSampleCount, 0, nullptr, nullptr);
    auto count = objects.size();
    ret = clEnqueueWriteBuffer(command_queue, kObjectCount, CL_TRUE, 0,
                               sizeof(int), &count, 0, nullptr, nullptr);
    auto lcount = lights.size();
    ret = clEnqueueWriteBuffer(command_queue, kLightCount, CL_TRUE, 0,
                               sizeof(int), &lcount, 0, nullptr, nullptr);
    ret = clEnqueueWriteBuffer(command_queue, kObjects, CL_TRUE, 0,
                               sizeof(RenderObject) * objects.size(),
                               objects.data(), 0, nullptr, nullptr);
    ret = clEnqueueWriteBuffer(command_queue, kLights, CL_TRUE, 0, sizeof(int) * lights.size(),
                               lights.data(), 0, nullptr, nullptr);
    ret = clEnqueueWriteBuffer(command_queue, kBVH, CL_TRUE, 0,
                               sizeof(BVHNode) * ((2 << MAX_BVH_DEPTH) + 1), bvhNode, 0, nullptr, nullptr);
    if (ret != CL_SUCCESS) {
        fprintf(stderr, "failed to write BVH tree\n");
        exit(-1);
    }
    fmt::print("Setting CL Args...\n");
    cl_uint arg = 0;
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kCamera);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kSampleCount);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kObjects);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kObjectCount);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kLights);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kLightCount);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kBVH);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kOutImage);
    ret = clSetKernelArg(kernel, arg++, sizeof(cl_mem), (void *) &kXi);
    global_item_size = w * h;

}

void Scene::resetRand() {
    srand((int) time(0));
    for (unsigned int i = 0; i < w * h * 2; i++) {
        seedBuffer[i] = (Seed) seed();
    }
    clEnqueueWriteBuffer(command_queue, kXi, CL_TRUE, 0,
                         sizeof(Seed) * w * h * 2,
                         seedBuffer, 0, nullptr, nullptr);
}

void Scene::constructBVH() {
    fmt::print("Constructing BVH tree...\n");
    bvhNode = new BVHNode[(2 << MAX_BVH_DEPTH) + 1];
    for (int i = 0; i < (2 << MAX_BVH_DEPTH) + 1; i++) {
        bvhNode[i].cnt = 0;
        for (int j = 0; j < MAX_BVH_CHILD; j++) {
            bvhNode[i].primitive[j] = -1;
        }
    }
    std::vector<RenderObject> vec = objects;
    topDownBVH(0, vec, 0, 0);
    // checks
    std::set<int> s;
    for (int i = 0; i < (2 << MAX_BVH_DEPTH) + 1; i++) {
        for (int j = 0; j < bvhNode[i].cnt; j++) {
            s.insert(bvhNode[i].primitive[j]);
        }
    }
    if (s.size() != objects.size()) {
        fmt::print(stderr,
                   "warning: error occurred in BVH, expected total objects {0}, actually have {1}",
                   objects.size(), s.size());
    }
}

static bool contains(const AABB &a, const AABB &b) {
    for (int i = 0; i < 3; i++) {
        if (a.min.s[i] - eps <= b.min.s[i] && b.max.s[i] <= a.max.s[i] + eps) {}
        else return false;
    }
    return true;
}

static AABB getBoundBox(const std::vector<RenderObject> &vec) {
    Vector vmin = vec3(inf, inf, inf), vmax = vec3(-inf, -inf, -inf);
    for (auto i:vec) {
        AABB box = getBoundBox(i);
        vmin = min(box.min, vmin);
        vmax = max(box.max, vmax);
    }
    Vector d = vec3(eps, eps, eps);
    vadd(vmax, d, vmax);
    vsub(vmin, d, vmin);
    AABB bb = (AABB) {.min = vmin, .max = vmax};
    for (auto i:vec) {
        AABB box = getBoundBox(i);
        //  fmt::print("check contains {}, {}",bb,box);
        assert(contains(bb, box));

    }
    return bb;
}

template<>
struct fmt::formatter<Vector> {
    template<typename ParseContext>
    constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

    template<typename FormatContext>
    auto format(const Vector &v, FormatContext &ctx) {
        return format_to(ctx.begin(), "({}, {}, {})", v.x, v.y, v.z);
    }
};

template<>
struct fmt::formatter<AABB> {
    template<typename ParseContext>
    constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

    template<typename FormatContext>
    auto format(const AABB &box, FormatContext &ctx) {
        return format_to(ctx.begin(), "[{}, {}]", box.min, box.max);
    }
};

static bool trySplit(AABB box, std::vector<RenderObject> &vec, int axis,
                     std::vector<RenderObject> &out1,
                     std::vector<RenderObject> &out2) {
    axis %= 3;
    std::sort(vec.begin(), vec.end(), [=](const RenderObject a, const RenderObject b) {
        return getAxis(center(getBoundBox(a)), axis)
               < getAxis(center(getBoundBox(b)), axis);
    });
    Vector pivot = getBoundBox(vec[vec.size() / 2]).max;
    out1.clear();
    out2.clear();
    for (auto i:vec) {
        if (getAxis(getBoundBox(i).max, axis) <= getAxis(pivot, axis)) {
            out1.emplace_back(i);
        } else {
            out2.emplace_back(i);
        }
    }
    if (out1.size() == vec.size() || out2.size() == vec.size()) {
        return false;
    }
    if (out1.empty() || out2.empty())
        return false;
    return true;
}

void Scene::topDownBVH(int ptr, std::vector<RenderObject> &vec, int axis, int depth) {
    if (ptr >= (2 << MAX_BVH_DEPTH) + 1) {
        fmt::print(stderr, "ptr exceeds MAX_BVH_DEPTH\n");
        fmt::print(stderr, "Cannot construct BVH Tree\n");
        exit(-1);
    }
    axis %= 3;
    if (vec.size() > MAX_BVH_CHILD) {
        std::vector<RenderObject> out1, out2;
        auto box = getBoundBox(vec);
        int a;
        for (a = axis; a < axis + 3; a++) {
            if (trySplit(box, vec, a, out1, out2)) {
                break;
            }
        }
        if (a == axis + 3) {
            fmt::print(stderr, "Split failed on all axis with depth={}, starting axis={} ", depth, axis);
            fmt::print(stderr, "{} objects left={}, right={}\n", vec.size(), out1.size(), out2.size());
            fmt::print(stderr, "Trying to force split\n");
            out1.clear();
            out2.clear();
            for (int i = 0; i < vec.size(); i++) {
                if (i % 2 == 0) {
                    out1.emplace_back(vec[i]);
                } else {
                    out2.emplace_back(vec[i]);
                }
            }
        }
        bvhNode[ptr].box = box;
        // fmt::print("next\n");
        topDownBVH(getLeft(ptr), out1, a + 1, depth + 1);
        topDownBVH(getRight(ptr), out2, a + 1, depth + 1);
    } else if (!vec.empty()) {
        bvhNode[ptr].box = getBoundBox(vec);
        bvhNode[ptr].cnt = vec.size();
        std::vector<int> trigs;
        std::vector<int> spheres;
        for (int i = 0; i < vec.size(); i++) {
            bvhNode[ptr].primitive[i] = vec[i].id;
        }
    }
}

RenderObject makeSphere(const Vector &center, double radius, const Material &material) {
    auto sphere = (Sphere) {.center = center,
            .radius = (float) radius};
    return (RenderObject) {.type = SPHERE,
            .sphere = sphere,
            .material = material,

    };
}

RenderObject makeTriangle(const Vector &v0, const Vector &v1, const Vector &v2, const Material &material) {
    auto trig = (Triangle) {.vertex0 = v0, .vertex1 = v1, .vertex2 = v2};
    return (RenderObject) {.type = TRIANGLE,
            .triangle = trig,
            .material = material,

    };
}

void Scene::loadObj(const std::string &filename) {
    fmt::print("Loading {0}\n", filename);
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string err;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename.c_str());

    if (!err.empty()) { // `err` may contain warning message.
        fmt::print(stderr, "{}\n", err);
    }

    if (!ret) {
        fmt::print(stderr, "cannot open file {}", filename);
        return;
    }
    fmt::print("Adding objects...\n");
    std::vector<Polygon> polygons;
// Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {
        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            int fv = shapes[s].mesh.num_face_vertices[f];
            auto id = shapes[s].mesh.material_ids[f];
            Material material;
            if (id >= 0 && id < materials.size()) {
                material.Ka.x = materials[id].ambient[0];
                material.Kd.x = materials[id].diffuse[0];
                material.Ks.x = materials[id].specular[0];
                material.Ka.y = materials[id].ambient[1];
                material.Kd.y = materials[id].diffuse[1];
                material.Ks.y = materials[id].specular[1];
                material.Ka.z = materials[id].ambient[2];
                material.Kd.z = materials[id].diffuse[2];
                material.Ks.z = materials[id].specular[2];
            } else {
                material.Ka.x = 0;
                material.Kd.x = 1;
                material.Ks.x = 0;
                material.Ka.y = 0;
                material.Kd.y = 1;
                material.Ks.y = 0;
                material.Ka.z = 0;
                material.Kd.z = 1;
                material.Ks.z = 0;
            }
            //fmt::print("{} {} {} {}\n",id, material.Ka,material.Kd,material.Ks);
            Polygon polygon;
            polygon.material = material;
            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++) {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
                tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
                tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
                /*   tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
                   tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
                   tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
                   tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
                   tinyobj::real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];*/
                polygon.addVertex(vec3(vx, vy, vz));
                // Optional: vertex colors
                // tinyobj::real_t red = attrib.colors[3*idx.vertex_index+0];
                // tinyobj::real_t green = attrib.colors[3*idx.vertex_index+1];
                // tinyobj::real_t blue = attrib.colors[3*idx.vertex_index+2];
            }
            polygons.emplace_back(polygon);
            index_offset += fv;
        }
    }
    fmt::print("Processing polygons...\n");
    for (auto &i:polygons) {
        auto trigs = i.getTrigs();
        for (auto &trig: trigs) {
            addObject(trig);
        }
    }
}
