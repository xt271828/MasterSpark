#include "kernel_header.cl"


__kernel
void render(__global Camera *_camera,
            __global const float *sampleCount,
            __global const RenderObject *objects,
            __global int *objectCount,
            __global const int *lights,
            __global int *lightCount,
            __global BVHNode *bvhNode,
            __global Vector *outImage,
            __global Seed *Xi) {
    unsigned int idx = get_global_id(0);
    Camera camera = *_camera;
    int w = (int) camera.w;
    int h = (int) camera.h;
    const float cnt = *sampleCount;
    float x, y;
    float dx, dy;
    dx = 2.0f / (float) w;
    dy = 2.0f / (float) h;
    x = (float) (idx % w) / w;
    y = ((float) (idx) / w) / h;
    x = 2 * x - 1;
    y = 2 * y - 1;
    if (idx >= w * h)
        return;
#ifdef PAINT
    unsigned int c = *sampleCount;
    c %= (w*h);
#else
    unsigned int c = idx;
    //updateSeed(Xi + c * 2);
#endif
    Seed seed[2];
    seed[0] = Xi[c * 2];
    seed[1] = Xi[c * 2 + 1];
    x += (2 * rand(seed) - 1 ) * dx;
    y += (2 * rand(seed) - 1 ) * dy;
    Vector origin, dir;
    origin = camera.origin;
    dir = normalize(vec3(x, y, 0) - vec3(0, 0, -1.5));
    dir = vecRotate(dir, vec3(1, 0, 0), camera.dir.y);
    dir = vecRotate(dir, vec3(0, 1, 0), camera.dir.x);
    Ray ray;
    ray.ro = origin;
    ray.rd = dir;
   outImage[idx] *= cnt;
    if (camera.GI) {
        Vector color = //radiance(ray, bvhNode, objects, lights, lightCount, seed);
               radianceDirect(ray, bvhNode, objects, lights, lightCount, seed);
        outImage[idx] +=  max(vec3(0,0,0),min(color,vec3(100,100,100)));
       //outImage[idx] += bidirectionalPathTracing(ray, bvhNode, objects, lights, lightCount, seed);
    } else {
        outImage[idx] += rayTrace(ray, bvhNode, objects,objectCount, seed);
    }
    outImage[idx] /= (1.0 + cnt);
    Xi[c * 2] = seed[0];
    Xi[c * 2 + 1] = seed[1];
}
