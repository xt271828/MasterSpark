//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_COMMON_H
#define MASTERSPARK_GPU_COMMON_H
#ifndef GPU_KERNEL
#define __kernel
#define __constant
#define __local
#define __global

#include <cmath>

#endif
#define clamp(x, a, b) ((x) < (a) ? (a) : ((x) > (b) ? (b) : (x)))
#define kmax(x, y) ( (x) > (y) ? (x) : (y))
#define kmin(x, y) ( (x) < (y) ? (x) : (y))
#define ksign(x) ((x) > 0 ? 1 : -1)
#ifndef GPU_KERNEL
#define SHARED inline
#else
#define SHARED
#endif
#define eps  (0.0001f)
#define inf  (1e5f)
#endif //MASTERSPARK_GPU_COMMON_H
