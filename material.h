//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_MATERIAL_H
#define MASTERSPARK_GPU_MATERIAL_H
#include "vector.h"
typedef enum MaterialType{
    EMIT,
    DIFF,
    SPEC
}MaterialType;
typedef struct Material{
    MaterialType type;
    Vector Ka;
    Vector Kd;
    Vector Ks;
}Material;
#endif //MASTERSPARK_GPU_MATERIAL_H
