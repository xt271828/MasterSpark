#include <iostream>
#include <stdio.h>
#include <stdlib.h>


#include "ptwindow.h"

#define MAX_SOURCE_SIZE (0x100000)

int box() {

    PathTracerWindow window(1000, 1000, 5000);
    //  window.addObject<Plane>(vec3{0, 0, 1}, vec3{0, 0, -1}, makeDiffuse(vec3{1, 1, 1}));
    //  window.addObject<Plane>(vec3{-1, 0, 0}, vec3{1, 0, 0}, makeDiffuse(vec3{0, 1, 0}));
    //  window.addObject<Plane>(vec3{1, 0, 0}, vec3{-1, 0, 0}, makeDiffuse(vec3{1, 0, 0}));
    // window.addObject<Plane>(vec3{0, -0.5, 0}, vec3{0, 1, 0}, makeDiffuse(vec3{1, 1, 1}));
    //  window.addObject<Sphere>(vec3{0, 0.8, 0}, 0.4, makeEmission(vec3{1, 1, 1}));
    //  window.addObject<Sphere>(vec3{-0.3, 0.4, 0}, 0.4, makeDiffuse(vec3{1, 1, 1}));
    window.setCameraPos(0, 0, -1);
    window.addObject(makeSphere(vec3(-100 + -1, 0, 0), 100, makeDiffuse(vec3(.75, .25, .25))));
    window.addObject(makeSphere(vec3(100 + 1, 0, 0), 100, makeDiffuse(vec3(.25, .25, .75))));
    window.addObject(makeSphere(vec3(0, 0, 100 + 3), 100, makeDiffuse(vec3(.75, .75, .75))));
    //window.addObject(makeSphere(vec3(0, 0, -2 - 100), 100, makeDiffuse(vec3(0, 0, 0))));
    window.addObject(makeSphere(vec3(0, -100 - 0.5, 0), 100, makeDiffuse(vec3(.75, .75, .75))));
    window.addObject(makeSphere(vec3(0, 100 + 1.5, 0), 100, makeDiffuse(vec3(.75, .75, .75))));
    window.addObject(makeSphere(vec3(0, 1.1, 1), 0.2, makeEmission(vec3(12, 12, 12))));
//    window.addObject(makeSphere(vec3(0, 0.8, 0), 0.4, makeEmission(vec3(1, 1, 1))));
//    window.addObject(makeSphere(vec3(-0.3, 0.4, 0), 0.4, makeDiffuse(vec3(1, 1, 1))));
//
//    window.addObject(makeSphere(vec3(-100 - 1, 0, 0), 100, makeDiffuse(vec3(0.8, 0, 0))));
//    window.addObject(makeSphere(vec3(100 + 1, 0, 0), 100, makeDiffuse(vec3(0, 0.8, 0))));
//    window.addObject(makeSphere(vec3(0, -100 - 0.5, 1), 100, makeDiffuse(vec3(0.8, 0.8, 0.8))));
//    window.addObject(makeSphere(vec3(0, 0, 1 + 100), 100, makeDiffuse(vec3(0.8, 0.8, 0.8))));
    window.show();
    return 0;
}

void uranus() {
    PathTracerWindow window(1000, 1000, 5000);
    window.addObject(makeSphere(vec3(10, 40, -10), 20,
                                makeEmission(vec3(1.5, 1.5, 1.5))));
    window.addObject(makeSphere(vec3(0, 0, 2), 1,
                                makeDiffuse(vec3(143.0f / 255.0f, 163.0f / 255.0f, 174.0f / 255.0f))));
    window.addObject(makeSphere(vec3(0, -100 - 1, 1), 100, makeDiffuse(vec3(0.8, 0.8, 0.8))));
    window.show();
}

void sphere() {
    PathTracerWindow window(1000, 1000, 5000);
    window.addObject(makeSphere(vec3(100, 0, -100), 1, makeEmission(vec3(10000, 10000, 10000))));
    // window.addObject<Sphere>(vec3{0,0,0},0.8,makeDiffuse(rgb(143,163,174)*0.9));
    for (float t = -3.1415 * 4; t < 3.1415 * 4; t += 3.1415 * 2 / 64) {
        window.addObject(makeSphere(vec3(sin(t) * 10, cos(t) * 1 + 1 * t, -cos(t) * 10), 0.5,
                                    makeDiffuse(vec3(143.0f / 255.0f, 163.0f / 255.0f, 174.0f / 255.0f))));
    }
    for (float t = -3.1415 * 4; t < 3.1415 * 4; t += 3.1415 * 2 / 128) {
        window.addObject(makeSphere(vec3(sin(t) * 20, cos(t) * 1 + 1 * t, -20 * cos(t)), 0.5,
                                    makeDiffuse(vec3(143.0f / 255.0f, 163.0f / 255.0f, 174.0f / 255.0f))));
    }
    window.show();
}

void triangleTest() {
    PathTracerWindow window(1000, 1000, 5000);
    Triangle triangle;
    triangle.vertex0 = vec3(-0.5, 0, 0);
    triangle.vertex1 = vec3(0.5, 0, 1);
    triangle.vertex2 = vec3(0, 1, 0.5);
    Vector a, b;
    vsub(triangle.vertex0, triangle.vertex1, a);
    vsub(triangle.vertex0, triangle.vertex2, b);
    vcross(a, b, triangle.norm);
    window.addObject((RenderObject) {.type = TRIANGLE, .triangle = triangle,
            .material=makeDiffuse(vec3(143.0f / 255.0f, 163.0f / 255.0f, 174.0f / 255.0f))});
    window.addObject(makeSphere(vec3(10, 0, -10), 10, makeEmission(vec3(1.5, 1.5, 1.5))));
    window.show();
}

void renderDefaultScene() {
    PathTracerWindow window(1000, 1000, 5000);
    window.setCameraPos(250, 200, -600);
    // window.addObject(makeSphere(vec3(253.0, 500.0, 332.0), 80, makeEmission(vec3(4,4,4))));
    window.loadObj("cornell_box.obj");
    window.show();
}

int main(int argc, char **argv) {
    glfwInit();
    if (argc < 2) {
       //  renderDefaultScene();;
        box();
        // sphere();
    }
    return 0;
}
