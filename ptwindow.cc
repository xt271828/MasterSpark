//
// Created by xiaoc on 2018/10/31.
//

#include "ptwindow.h"
#include "vector.h"
static std::unordered_map<GLFWwindow *, PathTracerWindow *> dispatcher;

void PathTracerWindow::save() {
    scene.save();
    lodepng::encode("out.png", scene.pixelBuffer, width, height);
}

void PathTracerWindow::resetSample() {
    scene.sampleResetFlag = true;
    start = sec();
}

void PathTracerWindow::render() {
    renderThread = std::thread([&]() {
        while (alive) {
            doRender();
        }
    });
    renderThread.detach();
}

void PathTracerWindow::doRender() {
   // scene.resetRand();
    if (scene.sampleCount < 100) {
        scene.renderSamples(spp, 1);
    }
    else {
        scene.resetRand();
        scene.renderSamples(spp, 100);
    }
    pixels = scene.pixelBuffer;
}

void PathTracerWindow::addToDispatch() {
    glfwSetKeyCallback(window, keyCallBack);
    dispatcher[window] = this;
}

void PathTracerWindow::keyEvent(int key, int scancode, int action, int mods) {
    Window::keyEvent(key, scancode, action, mods);
    Vector offset = vec3(0,0,0);
    static float scale = 1;
    if(key == GLFW_KEY_SPACE && action == GLFW_PRESS){
        resetSample();
        scene.camera.GI = ! scene.camera.GI;
    }
    if(key == GLFW_KEY_W && action == GLFW_PRESS){
        offset.z += 10;
        resetSample();
    }
    if(key == GLFW_KEY_S && action == GLFW_PRESS){
        offset.z -= 10;
        resetSample();
    }
    if(key == GLFW_KEY_A && action == GLFW_PRESS){
        offset.x -= 10;
        resetSample();
    }
    if(key == GLFW_KEY_D && action == GLFW_PRESS){
        offset.x += 10;
        resetSample();
    }
    if(key == GLFW_KEY_R && action == GLFW_PRESS){
        offset.y += 10;
        resetSample();
    }
    if(key == GLFW_KEY_F && action == GLFW_PRESS){
        offset.y -= 10;
        resetSample();
    }

    offset = vecRotate(offset, vec3(1, 0, 0), scene.camera.dir.y);
    offset = vecRotate(offset, vec3(0, 1, 0), scene.camera.dir.x);
    vsmul(offset,scale,offset);
    vadd(scene.camera.origin,offset,scene.camera.origin);
    if(key == GLFW_KEY_UP && action == GLFW_PRESS){
        scene.camera.dir.y -= 0.1;
        resetSample();
    }
    if(key == GLFW_KEY_DOWN && action == GLFW_PRESS){
        scene.camera.dir.y += 0.1;
        resetSample();
    }
    if(key == GLFW_KEY_RIGHT && action == GLFW_PRESS){
        scene.camera.dir.x += 0.1;
        resetSample();
    }
    if(key == GLFW_KEY_LEFT && action == GLFW_PRESS){
        scene.camera.dir.x -= 0.1;
        resetSample();
    }
    if(key == GLFW_KEY_Z && action == GLFW_PRESS){
        scale *= 0.1;
    }
    if(key == GLFW_KEY_X && action == GLFW_PRESS){
        scale /= 0.1;
    }
}

void PathTracerWindow::show() {
    start = sec();
    scene.prepare();
    window = glfwCreateWindow(width, height, "MasterSpark", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    if (!window) {
        glfwTerminate();
    }
    addToDispatch();
    resize();
    fmt::print("All set. MasterSparking...\n");
    render();
    while (!glfwWindowShouldClose(window)) {
        // doRender();
        update();
        glClear(GL_COLOR_BUFFER_BIT);
        paintGL();
        glfwSwapBuffers(window);
        glfwPollEvents();
        auto t = sec() - start;
        auto total = 1.0 / scene.sampleCount * spp * t;
        double sps = (double(scene.sampleCount
                             * scene.w
                             * scene.h) / t) / 1000.0;
        glfwSetWindowTitle(
                window,
                fmt::format("MasterSpark {3} ({0} spp) Elapsed: {1}s {2}kSamples/sec",
                            scene.sampleCount,
                            t, sps,
                            scene.camera.GI ? "GI" : ""
                ).c_str()


        );
        Sleep(1000 / 30);
    }
    glfwTerminate();
    alive = false;
    Sleep(500);
    save();
}

void keyCallBack(GLFWwindow *window, int key, int scancode, int action, int mods) {
    auto ptwindow = dispatcher[window];
    if(ptwindow){
        ptwindow->keyEvent(key,scancode, action,mods);
    }
}
