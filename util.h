//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_UTIL_H
#define MASTERSPARK_GPU_UTIL_H


#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>
#include <set>
#include <string>
#include <fstream>
#include <thread>
#include <ctime>
#include <random>
#include <unordered_map>
#include "lib/lodepng/lodepng.h"
#include "lib/tiny_obj_loader/tiny_obj_loader.h"
#include "fmt/format.h"
inline int toInt(double x){
    if(x<0)return 0;
    if(x>1){return 255;}
    return (int)(x * 255);
}
#endif //MASTERSPARK_GPU_UTIL_H
