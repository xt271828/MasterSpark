//
// Created by xiaoc on 2018/11/4.
//

#ifndef MASTERSPARK_GPU_CAMERA_H
#define MASTERSPARK_GPU_CAMERA_H
#include "vector.h"
typedef struct Camera{
    Vector origin;
    Vector dir;// dir is represented in euler angle;
    float w,h;
    bool GI;
}Camera;
#endif //MASTERSPARK_GPU_CAMERA_H
