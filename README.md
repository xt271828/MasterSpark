# MasterSpark
A GPU accelerated ~~bidirectional~~ path tracer using OpenCL.</br>Inspired by SmallptGPU(http://davibu.interfree.it/opencl/smallptgpu/smallptGPU.html)

# Controls
Space to switch from ray casting (for fast scene review) and path tracing.</br>
W,A,S,D,R,F move the camera</br>
Directional keys rotate the camera.</br>

# Features
1. BVH acceleration structure.
2. Direct light sampling (or Next Event Estimation)
3. Bidirectional path tracing (only need to get the weights right...)
4. Interactive interface
5. Obj files loading (using https://github.com/syoyo/tinyobjloader)
6. Iterative rendering
 

# Future Changes
1. Denoising
2. Texture
3. Better BVH construction

# Performance
On my personal laptop (XPS 13, Intel UHD Graphics 620, i7-8550U at 1.80GHz), it's 5 times faster than CPU OpenCL. It's even faster compared to ordinary CPU-based path tracer.</br>
When using next event estimation, the tracer is significantly slower (the bottleneck is BVH traversal). But its worthy since the output converges incredibly faster.

# Dependencies
GLFW3 and OpenCL sdk.</br>
gcc or clang is recommended. 

# Gallery
Direct light sampling (Next Event Estimation)
![Image](gallery/out.png)
Monte Carlo
![Image](gallery/cornell_box.png)
